#ifndef ROOTHISTCNV_PERSSVC_H
#define ROOTHISTCNV_PERSSVC_H 1

// Include files
#include "GaudiKernel/ConversionSvc.h"

// Forward declarations
class TFile;


namespace RootHistCnv {

/** @class RootHistCnv::PersSvc PersSvc.h

    Persistency service - to store histograms in ROOT format
    @author Charles Leggett
*/

  class PersSvc : public ConversionSvc {
  public:
    /// Initialise the service
    virtual StatusCode initialize();

    /// Finalise the service
    virtual StatusCode finalize();

    /// Convert a collection of transient data objects into another representation
    virtual StatusCode createRep(DataObject* pObject, IOpaqueAddress*& refpAddress);

    /// Standard constructor
    PersSvc( const std::string& name, ISvcLocator* svc );

    /// Standard destructor
    ~PersSvc() override = default;

  private:
    std::string m_defFileName;  ///< Default file name
    std::unique_ptr<TFile> m_hfile; ///< Pointer to the ROOT file
    bool m_prtWar = false;      ///< Already printed a Warning
    bool m_alphaIds;            ///< Force alphabetic histograms/ntuple IDs
    bool m_outputEnabled;       ///< Flag to enable/disable the output to file
  };

}    // namespace RootHistCnv

#endif    // ROOTHISTCNV_PERSSVC_H
